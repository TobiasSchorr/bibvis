# bibvis
Visualize data from .bib files.

## Dependencies

Data processing:
* numpy
* pandas
* bibtexparser

Plotting:
* matplotlib
* networkx

optional:
* pypandoc (converting latex expressions to plain text)

Tested with python version 3.8 and 3.9

## Quick setup
with conda:
``` python
    conda install numpy pandas matplotlib bibtexparser networkx pypandoc
```
    
or with pip:
``` python
    pip install numpy pandas matplotlib bibtexparser networkx pypandoc
```

## Examples

### Collaboration graph

Run via terminal
```shell
    ./bibvis/collaboration_graph.py your_bib_file_here.bib
```
Output will look somehow like that:
![example of collaboration_graph.py](./doc/README_files/example_collaboration_graph.png)

## License 
Copyright (C) 2021  Tobias Schorr (t.schorr@mailbox.org)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

A copy of the GNU General Public License can be found in the LICENSE file that comes along with this program, or see <http://www.gnu.org/licenses/>.
