#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  6 13:00:23 2021

Create a graph showing collaborations of authors.

Quickstart:
    with conda:
        >>> conda install numpy pandas matplotlib bibtexparser networkx pypandoc
    or with pip:
        >>> pip install numpy pandas matplotlib bibtexparser networkx pypandoc

@author: t.schorr@mailbox.org
"""
import sys
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import bibtexparser
import networkx as nx


def read_bibtex_file_to_df(path=None):
    if path is None:
        path = './bib_testfile.bib'
    try:
        with open(path) as bibtex_file:
            bib_database = bibtexparser.load(bibtex_file)
    except bibtexparser.bibdatabase.UndefinedString:
        with open(path) as bibtex_file:
            bib_database = bibtexparser.bparser.BibTexParser(common_strings=True).parse_file(bibtex_file)
    return pd.DataFrame(bib_database.entries)

   
def latex_to_unicode(latex_str):
    try:
        import pypandoc
        return pypandoc.convert_text(latex_str, 'plain', 'latex').replace('\n', ' ').strip()
    except ModuleNotFoundError():
        print("Warning: Module pypandoc not installed, latex expressions are not displayed properly.\n"
              "Install module `pypandoc` to resolve this issue.")
        return latex_str
    
    
def bibdf_split_authors_str2list(df):
    
    def split_multiple_authors_to_list(authors, author_sep=' and '):
        import re
        authors = re.split(author_sep, authors)
        return authors
        
    df.loc[:, 'author'] = list(map(split_multiple_authors_to_list, df.loc[:, 'author']))
    return df


def bibdf_shorten_authors_firstname(df):
    
    def change_name_format_from_commaseparated_to_spaceseparated(fullname):
        lastname, firstnames = fullname.split(',')
        return ' '.join([firstnames.strip(), lastname.strip()])
    
    def shorten_firstname(fullname):
        name_elements = fullname.split(' ')
        
        # ensure that an already shortened firstname is abbreviated with a dot
        for i, firstname in enumerate(name_elements[:-1]):
            if len(firstname) == 1:
                name_elements[i] = firstname + '.'
        
        # shorten firstnames with a dot
        for i, firstname in enumerate(name_elements[:-1]):
            if firstname[-1] != '.':
                name_elements[i] = firstname[0] + '.'
        
        return ' '.join(name_elements)
    
    for publication in df.itertuples():
        pub_authors = publication.author
        for i, single_author in enumerate(pub_authors):
            if ',' in single_author:
                single_author = change_name_format_from_commaseparated_to_spaceseparated(single_author)
            pub_authors[i] = shorten_firstname(single_author)
        df.loc[publication.Index, 'author'] = pub_authors
        
    return df
    

def bibdf_count_authors(df):
    df['author_num'] = df['author'].map(len)
    return df


def get_author_list(df):
    authors_entries = np.array([])
    for paper_authors in df['author'].to_numpy():
        for single_author in paper_authors:
            authors_entries = np.append(authors_entries, single_author)
    return pd.Series(authors_entries).sort_values()


# %% create collaboration matrx

def get_collaboration_count_matrix(df):
    collab_matrix = pd.DataFrame(data=0, index=get_author_list(df).unique(), columns=get_author_list(df).unique())

    for publication in df.itertuples():
        pub_authors = publication.author
        
        for single_author in pub_authors:
            for other_authors in pub_authors:
                collab_matrix.at[single_author, other_authors] += 1
    
    return collab_matrix


# %% visualize collaboration matrix

def generate_graph(cm):
    """ Create adjacency matrix wrt author collaboration"""
    G = nx.from_numpy_matrix(cm.to_numpy())
    G = nx.relabel_nodes(G, dict(enumerate(cm.columns)))
    return G


# %% Create figure
def plot_graph(G, cm, fig_width, layout='spring', file_path=None):
    
    def get_node_positions(G, layout):
        if layout == 'spring':
            pos = nx.spring_layout(G, k=0.2)
        elif layout in ['circular', 'circle']:
            pos = nx.circular_layout(G)
        elif layout == 'shell':
            pos = nx.shell_layout(G)
        return pos
    
    G.remove_edges_from(nx.selfloop_edges(G))
    pos = get_node_positions(G, layout)
    
    size_ratio = 16 / 9
    fig_height = fig_width / size_ratio
    fig = plt.figure(figsize=[fig_width, fig_height])
    if file_path:
        fig.suptitle(f"Collaboration graph for .bib file '{file_path}'", fontsize=fig_width)
    ax = fig.add_subplot(111)
    ax.patch.set_facecolor('k')
    
    # def color code for nodes and edges
    weights = list()
    for u, v in G.edges():
        weights.append(G.get_edge_data(u, v, default=0)["weight"])
    norm_edges = plt.Normalize(vmin=1, vmax=5)
    cmap_edges = plt.get_cmap('cool')
    
    norm_nodes = plt.Normalize(vmin=1, vmax=cm.to_numpy().max())
    cmap_nodes = plt.get_cmap('winter')
            
    nx.draw_networkx_nodes(G, pos=pos, ax=ax,
                           node_size=cm.to_numpy().diagonal() * 10 ** 2,
                           node_color=cmap_nodes(norm_nodes(cm.to_numpy().diagonal())),)
    
    # plt.rcParams.update({"text.usetex": True})
    nx.draw_networkx_labels(G, pos=pos, ax=ax,
                            font_color='k',
                            font_size=10,
                            bbox=dict(ec="k", fc="lightgrey", boxstyle='round', alpha=0.3),)
    
    nx.draw_networkx_edges(G, pos=pos, ax=ax,
                           edge_color=cmap_edges(norm_edges(weights)),
                           width=2,
                           alpha=0.1
                           )
    
    # Create colorbars
    fig_dim = ax.get_position().get_points()
    x0, y0 = (fig_dim[0][0], fig_dim[0][1])
    x1, y1 = (x0 + fig_dim[1][0], y0 + fig_dim[1][1])
    cbar_thickness = 0.02
    
    cbar_thickness_nodes = cbar_thickness / (y1 - y0)
    # cax_nodes = ax.inset_axes([x1 + cbar_thickness_nodes, y0, cbar_thickness_nodes, y1 - y0], label='cax_nodes')
    cax_nodes = ax.inset_axes([1.02, 0, cbar_thickness_nodes, 1], label='cax_nodes')
    mapper_nodes = plt.cm.ScalarMappable(cmap=cmap_nodes, norm=norm_nodes)
    fig.colorbar(mapper_nodes, label='# Authorship',
                 cax=cax_nodes
                 )
    
    x0, x1, y0, y1 = (fig_dim[0][0], fig_dim[1][0], fig_dim[0][1], fig_dim[1][1])
    cbar_thickness_edges = cbar_thickness * size_ratio
    # cax_edges = ax.inset_axes([x0, y0 - cbar_thickness_edges * 2, x1 - x0, cbar_thickness_edges], label='cax_nodes')
    cax_edges = ax.inset_axes([x0, -cbar_thickness_edges * 1.5, x1 - x0, cbar_thickness_edges], label='cax_nodes')
    mapper_edges = plt.cm.ScalarMappable(cmap=cmap_edges, norm=norm_edges)
    
    fig.colorbar(mapper_edges, label='# Collaborations between two persons', orientation='horizontal',
                 cax=cax_edges
                 )
    
    plt.tight_layout()
    return fig


# %% apply all

def main(path_to_bib):
    """Generates a social graph figure showing collaborations from .bib file"""
    bib_df = read_bibtex_file_to_df(path_to_bib)
    bib_df = bib_df[bib_df.author.notna()]
    
    # change latex expressions to plain text
    mask_latex = bib_df.author.str.contains(r'\\')
    bib_df.loc[bib_df[mask_latex].index, 'author'] = list(map(latex_to_unicode, bib_df[mask_latex].author))
    
    bib_df = bibdf_split_authors_str2list(bib_df)
    bib_df = bibdf_shorten_authors_firstname(bib_df)
    collab_adjacency_matrix = get_collaboration_count_matrix(bib_df)
    G = generate_graph(collab_adjacency_matrix)
    
    plot_graph(G, collab_adjacency_matrix, fig_width=20, layout='spring', file_path=path_to_bib)
    plt.show()


def parse_args_to_file_list():
    if len(sys.argv) == 1:
        bib_file_paths = [input("WARNING: You didn't specified a .bib file, yet. Plaese enter a path here:\n")]
    elif len(sys.argv) == 2:
        bib_file_paths = [sys.argv[1]]
    elif len(sys.argv) > 2:
        bib_file_paths = list()
        for arg in sys.argv[1:]:
            bib_file_paths.append(arg)
    return bib_file_paths
            
        
if __name__ == "__main__":
    bib_file_paths = parse_args_to_file_list()
        
    for path in bib_file_paths:
        main(path)
